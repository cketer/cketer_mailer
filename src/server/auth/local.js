const moment = require('moment');
// sign with default (HMAC SHA256)
const jwt = require('jsonwebtoken');


function encodeToken(user) {
  const playload = {
    exp: moment().add(30, 'minutes').unix(),
    iat: moment().unix(),
    sub: user.id
  };
  return jwt.sign(playload, process.env.TOKEN_SECRET, { });
}

function decodeToken(token, callback) {

  const payload = jwt.verify(token, process.env.TOKEN_SECRET);

  const now = moment().unix();
  // check if the token has expired
  if (now > payload.exp) callback('Token has expired.');
  else callback(null, payload);
}

module.exports = {
  encodeToken,
  decodeToken
};
