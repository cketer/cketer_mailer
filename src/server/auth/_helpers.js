const bcrypt = require('bcryptjs');
const knex = require('../db/connection');
const localAuth = require('./local');
const moment=require('moment');


function createUser(req) {

  const salt = bcrypt.genSaltSync();
  const hash = bcrypt.hashSync(req.body.password, salt);
  var date=moment();

  return knex('users')
    .insert({
      fullname: req.body.fullname,
      created_on:date,
      username: req.body.username,
      password: hash
    })
    .returning('*');
}


function updateProfile(req) {
  var userName = req.params.id;
  return knex('users')
    .where('username', userName)
    .update({
      fullname: req.body.fullname,
    })
    .returning('*');

}


function updatePassword(req) {
  var userName = req.body.username;
  const salt = bcrypt.genSaltSync();
  const hash = bcrypt.hashSync(req.body.password, salt);
  return knex('users')
    .where('username', userName)
    .update({
      password: hash
    })
    .returning('*');
}


function getUser(username) {
  return knex('users').where({username}).first();
}

function comparePass(databasePassword, userPassword) {
  const bool = bcrypt.compareSync(databasePassword, userPassword);
  if (!bool) throw new Error('bad password!');
  else return true;
}

function ensureAuthenticated(req, res, next) {
  if (!(req.headers && req.headers.authorization)) {
    return res.status(400).json({
      status: 'Please log in'
    });
  }
  // decode the token
  var header = req.headers.authorization.split(' ');
  var token = header[1];
  localAuth.decodeToken(token, (err, payload) => {
    if (err) {
      return res.status(401).json({
        status: 'Token has expired'
      });
    } else {

      // check if the user still exists in the db
      return knex('users').where({id: parseInt(payload.sub)}).first()
        .then((user) => {
          next();
        })
        .catch((err) => {
          res.status(500).json({
            status: 'error'
          });
        });
    }
  });
}

module.exports = {
  createUser,
  updateProfile,
  updatePassword,
  getUser,
  comparePass,
  ensureAuthenticated
};
