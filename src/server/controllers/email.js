const nodemailer = require('nodemailer');
const moment = require('moment');
const knex = require('../db/connection');

 async function sendEmail(req,res, next) {
   // Request details
   let email = req.body.email;
   let fullname = req.body.fullname;
   let  message = req.body.message;
   let email_password = process.env.EMAIL_PASSWORD

   //Transporter
  let transporter = nodemailer.createTransport({
    host: 'smtp.zoho.com.au',
    port: 587,
    secure: false,
    auth: {
      user: 'tech@collinsketer.com',
      pass: email_password
    }
  });

  // Mail details
  let mailOptions = {
    from: '" 👻 - Website Message from: "<tech@collinsketer.com>',
    to: "info@collinsketer.com",
    subject: "New Message from user",
    // text: "This displays text but I prefer using HTML AS I have more control in customizing the look and feel of the message",
    html: "<p> <strong>Message from: <em>" +fullname+"</em></en></strong><br><br>" + message + "<br><br><br> You can reach the sender via:<br><br> " + email+"</p>"
  }

  // Call transporter and pass in mail details

   await transporter.sendMail(mailOptions);

}
module.exports = {
  sendEmail
};

