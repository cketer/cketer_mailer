const knex = require('../db/connection');


 function getAllUsers(){
  return knex.select().table('users');
}

function getUser(username) {
  return knex('users').where({ username }).first();
}

function updateUser(req) {
  var userID = req.params.id;
  return knex('users')
    .where('username', userID)
    .update({
      fullname: req.body.fullname,
      email: req.body.email,
      type:req.body.type
    });
}

module.exports = {
  getAllUsers,
  getUser,
  updateUser
};
