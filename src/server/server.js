(function() {

  'use strict';

  const app = require('./app');
  const debug = require('debug')('herman-express:server');
const fs = require('fs');
//const http = require('http');
const https = require('https');

 const privateKey = fs.readFileSync('/etc/letsencrypt/live/www.collinsketer.com/privkey.pem', 'utf8');
 const certificate = fs.readFileSync('/etc/letsencrypt/live/www.collinsketer.com/cert.pem', 'utf8');
 const ca = fs.readFileSync('/etc/letsencrypt/live/www.collinsketer.com/chain.pem', 'utf8');

 const credentials = {
 	key: privateKey,
 	cert: certificate,
 	ca: ca
 };

  const port = normalizePort(process.env.PORT || '3001');


  app.set('port', port);

   const server = https.createServer(credentials,app);
//  const server = http.createServer(app);

  server.listen(port);
  server.on('error', onError);
  server.on('listening', onListening);

  function normalizePort(val) {
    const port = parseInt(val, 10);
    if (isNaN(port)) {
      return val;
    }
    if (port >= 0) {
      return port;
    }
    return false;
  }

  function onError(error) {
    if (error.syscall !== 'listen') {
      throw error;
    }
    const bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;
    switch (error.code) {
      case 'EACCES':
        console.error(bind + ' requires elevated privileges');
        process.exit(1);
        break;
      case 'EADDRINUSE':
        console.error(bind + ' is already in use');
        process.exit(1);
        break;
      default:
        throw error;
    }
  }

  function onListening() {
    const addr = server.address();
    const bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
    debug('Listening on ' + bind);
  }
}());
