const express = require('express');
const router = express.Router();
const authHelpers = require('../auth/_helpers');
const usR = require('../controllers/user');


router.get('/users',
authHelpers.ensureAuthenticated,
  (req, res, next)  => {
    var agentCode = req.query.agentCode;
    return usR.getAllUsers(agentCode)
    .then((data) =>{
       res.status(200).json({
        status: 'success',
        data: data,
        message: 'Retrieved ALL users'
      });
    })

});

router.get('/users/:id',
 authHelpers.ensureAuthenticated,
  (req, res, next)  => {
    var agentID = req.params.id;
    return usR.getUser(agentID)
    .then((data) =>{
       res.status(200).json({
        status: 'success',
        data: data,
        message: 'Retrieved single user'
      });
    })

});


router.put('/users/:id',
  authHelpers.ensureAuthenticated,
  (req, res, next)  => {
    return usR.updateUser(req)
    .then(() =>{
       res.status(200).json({
        status: 'success',
        message: 'Updated user item'
      });
    });

});

module.exports = router;
