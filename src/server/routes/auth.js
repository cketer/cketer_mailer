const express = require('express');
const router = express.Router();
const localAuth = require('../auth/local');
const authHelpers = require('../auth/_helpers');

//
// /**
//  * @swagger
//  * definitions:
//  *   User:
//  *     properties:
//  *       category:
//  *         type: string
//  *       api_user:
//  *         type: string
//  *       file:
//  *         type: file
//  */


router.post('/register', (req, res, next)  => {
  return authHelpers.createUser(req)
  .then((user) => { return localAuth.encodeToken(user[0]); })
  .then((token) => {
    res.status(200).json({
      status: 'success',
      token: token
    });
  })
    /*
  
  .catch((err) => {
    res.status(500).json({
      status: 'error'
    });
  }) */;  
});

router.put('/resettemppassword',
  authHelpers.ensureAuthenticated,
   (req, res, next)  => {
    var Semail=req.body.email;
     return authHelpers.updatePassword(req)
     .then(() =>{
        res.status(200).json({
         status: 'success',
         message: 'Updated user password'
       });
     });
 });

router.put('/updateuser/:id',
  authHelpers.ensureAuthenticated,
  (req, res, next)  => {
  return authHelpers.updateProfile(req)
    .then(() =>{
       res.status(200).json({
        status: 'success',
        message: 'Updated user profile details'
      });
    })
});

/**
 * @swagger
 * /auth/login:
 *   post:
 *     tags:
 *       - login
 *     description: login user
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: user
 *         description: user object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/User'
 *     responses:
 *       200:
 *         description: Successfully created
 */
router.post('/login', (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;
  return authHelpers.getUser(username)
  .then((response) => {
    authHelpers.comparePass(password, response.password);
    return response;
  })
  .then((response) => {
      return localAuth.encodeToken(response);
      })
  .then((token) => {
    res.status(200).json({
      status: 'success',
      token: token

    });
  })
  .catch((err) => {
    res.status(500).json({
      status: 'error',
        message: err.message

    });

  });
});

/**
 * @swagger
 * /auth/user/{id}:
 *   get:
 *     tags:
 *       - user
 *     description: Returns a single user
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: user's id
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: A single user
 *         schema:
 *           $ref: '#/definitions/User'
 */
router.get('/user',
  authHelpers.ensureAuthenticated,
  (req, res, next)  => {
  res.status(200).json({
    status: 'success',
  });
});

module.exports = router;
