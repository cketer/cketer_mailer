const express = require('express');
const router = express.Router();
const emailR = require('../controllers/email');
const authHelpers = require('../auth/_helpers');



router.post('/email',
  authHelpers.ensureAuthenticated,
  (req, res, next)  => {
    return emailR.sendEmail(req)
      .then(() =>{
        res.status(200).json({
          status: 'success',
          message: 'Email sent'
        });
      });
  });

module.exports = router;
