exports.up = (knex, Promise) => {
    return knex.schema.createTable('users', (table) => {
      table.increments();
      table.string('fullname');
      table.string('username').unique().notNullable();
      table.string('password').notNullable();
      table.string('created_on',50);
    });
  };
  
  exports.down = (knex, Promise) => {
    return knex.schema.dropTable('users');
  };
  