const bcrypt = require('bcryptjs');

exports.seed = (knex, Promise) => {
  return knex('users').del()
  .then(() => {
    const salt = bcrypt.genSaltSync();
    const hash = bcrypt.hashSync('Tyme4ch@n9e', salt);
    return Promise.join(
      knex('users').insert({
        username: 'apijambousr',
        password: hash
      })
    );
  });
};
