(function (routeConfig) {

  'use strict';

  routeConfig.init = function (app) {
    // *** routes *** //
    const routes = require('../routes/index');
   //Authentication routes
    const authRoutes = require('../routes/auth');

    //const loginRoutes = require('../routes/login');
     const userRoutes = require('../routes/user');
     const emailRoutes = require('../routes/email');
   // *** register routes *** //
     app.use('/', routes);
     //app.use('/login', loginRoutes);
     app.use('/auth', authRoutes);
     app.use('/api', userRoutes);
     app.use('/api', emailRoutes);

  };

})(module.exports);
