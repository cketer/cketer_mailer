
(function(appConfig) {
  'use strict';

  // *** main dependencies *** //
  const path = require('path');
  const cookieParser = require('cookie-parser');
  const bodyParser = require('body-parser');
  const session = require('express-session');
  const flash = require('connect-flash');
  const morgan = require('morgan');
  const nunjucks = require('nunjucks');
  const bunyan = require('bunyan');


  // *** view folders *** //
  const viewFolders = [
    path.join(__dirname, '..', 'views')
  ];

  // *** load environment variables *** //
  require('dotenv').config();

  appConfig.init = function(app, express) {

     //options for the swagger docs
  const options = {
    // import swaggerDefinitions
    swaggerDefinition: {
      info: {
        title: 'cketer mailer API',
        version: '1.0.0',
        description: 'cketer mailer to send email from my website collinsketer.com',
        contact: {
          email: 'collinsketer@gmail.com, cketer@collinsketter.com'
        }
      },
      tags: [
        {
         
        }
        
      ],
      schemes: ['https'],
      host: 'www.collinsketer.com:3001',
      basePath: '/'
    },
    // path to the API docs
    apis: ['./src/server/routes/*.js'],
    customCss: '#header { display: none }'
  };

    //Bunyan logging
    app.use(require('express-bunyan-logger')({name: 'Collins Keter Mailer API',
      streams: [{
        type: 'rotating-file',
        level: 'info',                  // logging level
        path: './src/server/public/logs/api.log',
        period: '1d',   // daily rotation
        count: 3,        // keep 3 back copies
      }]
    }));

  // initialize swagger-jsdoc
  const swaggerJSDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')
const swaggerSpec = swaggerJSDoc(options)
 //console.log(options);

  app.get('/swagger.json', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
    console.log(swaggerSpec);
  });
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));



    // *** view engine *** //
    nunjucks.configure(viewFolders, {
      express: app,
      autoescape: true
    });
    app.set('view engine', 'html');

    // *** app middleware *** //
    if (process.env.NODE_ENV !== 'test') {
      app.use(morgan('dev'));
    }
    app.use(cookieParser());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    // // uncomment if using express-session
    // app.use(session({
    //   secret: process.env.SECRET_KEY,
    //   resave: false,
    //   saveUninitialized: true
    // }));
    app.use(flash());
    app.use(express.static(path.join(__dirname, '..', '..', 'server','public','Uploads')));




    app.use(function (req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Authorization, Accept, Cache-Control");
      res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
      next();
    });

  };

})(module.exports);
